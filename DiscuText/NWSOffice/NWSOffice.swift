//
//  NWSOffice.swift
//  DiscuText
//
//  Created by Andrew Fink on 9/9/18.
//  Copyright © 2018 DiscuText. All rights reserved.
//

import Foundation

import ObjectMapper

struct NWSOffice: Comparable, CustomStringConvertible, Mappable {
  var cwa: String?
  var name: String?
  var region: String?
  var state: String?
  var stateAbbrev: String?
  var wfo: String?
  var lat: Double?
  var lon: Double?

  var description: String {
    return "NWSOffice: \(cwa ?? "???"), \(name ?? "---")"
  }

  var label: String {
    if let n = name, let s = stateAbbrev {
      return "\(n), \(s)"
    } else if let n = name {
      return "\(n)"
    } else if let c = cwa {
      return c
    } else {
      return "---"
    }
  }

  init() {}

  init?(map: Map) {}

  mutating func mapping(map: Map) {
    cwa <- map["CWA"]
    name <- map["City"]
    region <- map["Region"]
    state <- map["State"]
    stateAbbrev <- map["ST"]
    wfo <- map["WFO"]
    lat <- map["LAT"]
    lon <- map["LON"]
  }

  static func < (lhs: NWSOffice, rhs: NWSOffice) -> Bool {
    if lhs.stateAbbrev != rhs.stateAbbrev {
      if let lStateAbbrev = lhs.stateAbbrev, let rStateAbbrev = rhs.stateAbbrev {
        return lStateAbbrev < rStateAbbrev
      } else if lhs.stateAbbrev != nil {
        return true
      } else {
        return false
      }
    } else {
      if let lName = lhs.name, let rName = rhs.name {
        return lName < rName
      } else if lhs.name != nil {
        return true
      } else {
        return false
      }
    }
  }

  static func == (lhs: NWSOffice, rhs: NWSOffice) -> Bool {
    return lhs.wfo == rhs.wfo
  }
}
