//
//  Discussion.swift
//  DiscuText
//
//  Created by Andrew Fink on 9/12/18.
//  Copyright © 2018 DiscuText. All rights reserved.
//

import Foundation

import ObjectMapper

struct DiscussionSection: Mappable {
  var header: String = ""
  var text: String = ""

  init?(map: Map) {}

  mutating func mapping(map: Map) {
    header <- map["header"]
    text <- map["text"]
  }
}

struct Discussion: CustomStringConvertible, Mappable{
  var wfo: String = ""
  var validAt: Int = 0
  var sections: [DiscussionSection] = []
  var rawText = ""

  var description: String {
    return "Discussion: \(wfo), \(validAt)"
  }

  init() {}

  init?(map: Map) {}

  mutating func mapping(map: Map) {
    wfo <- map["wfo"]
    validAt <- map["valid_at"]
    sections <- map["sections"]
    rawText <- map["text"]
  }
}
