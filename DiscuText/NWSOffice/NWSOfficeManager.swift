//
//  NWSOfficeManager.swift
//  DiscuText
//
//  Created by Andrew Fink on 9/10/18.
//  Copyright © 2018 DiscuText. All rights reserved.
//

import Foundation

import Alamofire
import AlamofireObjectMapper
import GEOSwift
import ObjectMapper
import PromiseKit

class UnknownError : Error {}

struct NWSOfficeManagerError : Error {
  var description: String
}

class NWSOfficeManager {
  static func get(wfo: String) -> Promise<NWSOffice> {
    return firstly {
      NWSOfficeManager.list()
    }.firstValue { office in
      office.wfo == wfo
    }
  }

  static func getForPoint(latitude: Double, longitude: Double) -> NWSOffice? {
    if let path = Bundle.main.url(forResource: "county-warning-areas", withExtension: "geojson"),
        let features = try! Features.fromGeoJSON(path),
        let searchPoint = Waypoint(latitude: latitude, longitude: longitude) {
      let feature = features.first(where: {$0.geometries?.first?.contains(searchPoint) ?? false})
      if let properties = feature?.properties,
          let office = Mapper<NWSOffice>().map(JSON: properties as! [String : Any]) {
        print("Found office: \(office)")
        return office
      }
    }
    return nil
  }

  static func list() -> Promise<[NWSOffice]> {
    return Promise { seal in
      Alamofire.request("https://api.discutext.com/nws-office")
               .validate(statusCode: 200..<201)
               .responseArray { (response: DataResponse<[NWSOffice]>) in
        if let offices = response.result.value {
          seal.fulfill(offices.sorted())
        } else if let error = response.result.error {
          seal.reject(error)
        } else {
          // Here's a classic "this should never happen" comment
          seal.reject(UnknownError())
        }
      }
    }
  }

  static func search(text: String) -> Promise<[NWSOffice]> {
    return firstly {
      NWSOfficeManager.list()
    }.map { offices in
      offices.filter { office in
        let lower_text = text.lowercased()
        if let name = office.name, name.lowercased().contains(lower_text) {
          return true
        }
        if let wfo = office.wfo, wfo.lowercased().contains(lower_text) {
          return true
        }
        return false
      }
    }
  }

  static func latest(wfo: String) -> Promise<Discussion> {
    return Promise { seal in
      Alamofire.request("https://api.discutext.com/discussion/\(wfo)/latest")
               .validate(statusCode: 200..<201)
               .responseObject { (response: DataResponse<Discussion>) in
        if let discussion = response.result.value {
          seal.fulfill(discussion)
        } else if let error = response.result.error {
          seal.reject(error)
        } else {
          seal.reject(UnknownError())
        }
      }
    }
  }
}
