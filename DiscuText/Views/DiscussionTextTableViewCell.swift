//
//  DiscussionTextTableViewCell.swift
//  DiscuText
//
//  Created by Andrew Fink on 9/12/18.
//  Copyright © 2018 DiscuText. All rights reserved.
//

import UIKit

class DiscussionTextTableViewCell: UITableViewCell {

  static let IDENTIFIER = "DiscussionTextTableViewCell"

  @IBOutlet weak var textView: UITextView!
}
