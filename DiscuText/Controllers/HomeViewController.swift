//
//  HomeViewController.swift
//  DiscuText
//
//  Created by Andrew Fink-Miller on 12/3/18.
//  Copyright © 2018 DiscuText. All rights reserved.
//

import UIKit
import PromiseKit
import SkeletonView

let USER_DEFAULTS_OFFICE_WFO = "com.discutext.officeWfo"

fileprivate extension Selector {
  static let didBecomeActive = #selector(HomeViewController.didBecomeActive)
}

class HomeViewController: UIViewController {

  @IBOutlet weak var alertButton: UIButton!
  @IBOutlet weak var dateDotLabel: UILabel!
  @IBOutlet weak var favoriteButton: UIButton!
  @IBOutlet weak var helpView: UIView!
  @IBOutlet weak var locateButton: UIBarButtonItem!
  @IBOutlet weak var nameLabel: UILabel!
  @IBOutlet weak var officeControlsStack: UIStackView!
  @IBOutlet weak var officeNameDateStack: UIStackView!
  @IBOutlet weak var officeView: UIView!
  @IBOutlet weak var rawTextArea: UITextView!
  @IBOutlet weak var validAtLabel: UILabel!

  var isLoading = false {
    didSet {
      if isLoading {
        rawTextArea.isSelectable = false
        officeControlsStack.isHidden = true
        officeNameDateStack.isHidden = true
        officeView.showSkeleton()
      } else {
        officeView.hideSkeleton()
        officeControlsStack.isHidden = false
        officeNameDateStack.isHidden = false
        rawTextArea.isSelectable = true
      }
    }
  }

  var wfo: String = "PHI" {
    didSet {
      clearHelpView()
      getForWfo(wfo: wfo)
    }
  }

  var office: NWSOffice? {
    didSet {
      if let o = office {
        nameLabel.text = o.label
        let defaults = UserDefaults.standard
        if let wfo = o.wfo {
          defaults.set(wfo, forKey: USER_DEFAULTS_OFFICE_WFO)
        }
      } else {
        nameLabel.text = "---"
      }
    }
  }

  var discussion: Discussion? {
    didSet {
      if let d = discussion {
        let date = Date(timeIntervalSince1970: Double(d.validAt))
        let now = Date()

        // Set time dot color
        let hourDifference = now.hours(from: date)
        if hourDifference < 3 {
          dateDotLabel.textColor = UIColor(rgb: 0x28a745)
        } else if hourDifference < 6 {
          dateDotLabel.textColor = UIColor(rgb: 0xffc107)
        } else {
          dateDotLabel.textColor = UIColor(rgb: 0xdc3545)
        }

        // Set datetime string
        let dateFormatter = DateComponentsFormatter()
        dateFormatter.unitsStyle = .full
        var components = DateComponents()
        components.hour = hourDifference
        if let dateString = dateFormatter.string(from: components) {
          validAtLabel.text = "\(dateString) ago"
        } else {
          let dateFormatter = DateFormatter()
          dateFormatter.dateStyle = .short
          dateFormatter.timeStyle = .short
          dateFormatter.timeZone = TimeZone.current
          validAtLabel.text = dateFormatter.string(from: date)
        }

        // Set discussion text
        rawTextArea.text = d.rawText
        rawTextArea.scrollRangeToVisible(NSRange(location: 0, length: 0))
      }
    }
  }

  @IBAction func onLocateClicked(_ sender: UIBarButtonItem) {
    CLLocationManager.requestLocation(authorizationType: .whenInUse,
                                      satisfying: { $0.horizontalAccuracy < 1000 }).done { locations in
      if let location = locations.first,
         let office = NWSOfficeManager.getForPoint(latitude: location.coordinate.latitude,
                                                   longitude: location.coordinate.longitude),
         let wfo = office.wfo {
        self.wfo = wfo
      }
    }.catch { error in
      self.locateButton.isEnabled = false
    }
  }

  @objc func didBecomeActive() {
    updateLocateButtonInteractivity()
  }

  private func updateLocateButtonInteractivity() {
    let isLocateEnabled = [.authorizedAlways, .authorizedWhenInUse, .notDetermined]
      .contains(CLLocationManager.authorizationStatus())
    locateButton.isEnabled = isLocateEnabled
  }

  private func clearHelpView() {
    self.view.bringSubviewToFront(officeView)
  }

  private func showHelpView() {
    self.view.bringSubviewToFront(helpView)
  }

  private func getForWfo(wfo: String) {
    isLoading = true
    let waitAtLeast = after(seconds: 0.5)
    firstly {
      waitAtLeast
    }.then {
      when(fulfilled: NWSOfficeManager.get(wfo: wfo), NWSOfficeManager.latest(wfo: wfo))
    }.done { (office, discussion) in
      self.office = office
      self.discussion = discussion
      self.isLoading = false
    }
  }
}

extension HomeViewController {
  override func viewDidLoad() {
    super.viewDidLoad()

    NotificationCenter.default.addObserver(self,
                                           selector: .didBecomeActive,
                                           name: UIApplication.didBecomeActiveNotification,
                                           object: nil)

    let defaults = UserDefaults.standard
    if let wfo = defaults.string(forKey: USER_DEFAULTS_OFFICE_WFO) {
      self.wfo = wfo
    } else {
      showHelpView()
    }
  }

  // MARK: - Navigation

  override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
    if segue.identifier == "OfficeListSegue", let destination = segue.destination as? OfficeListViewController {
      destination.nwsOfficeSelectedDelegate = self
    }
  }
}

extension HomeViewController: NWSOfficeSelectedDelegate {
  func onOfficeSelected(office: NWSOffice) {
    if let wfo = office.wfo {
      self.wfo = wfo
    }
  }
}
