//
//  OfficeListViewController.swift
//  DiscuText
//
//  Created by Andrew Fink-Miller on 12/3/18.
//  Copyright © 2018 DiscuText. All rights reserved.
//

import UIKit

import PromiseKit

class OfficeListViewController: UIViewController {

  @IBOutlet weak var tableView: UITableView!
  weak var nwsOfficeSelectedDelegate: NWSOfficeSelectedDelegate?

  var searchResults: [NWSOffice] = [] {
    didSet {
      tableView.reloadData()
    }
  }
  var offices: [NWSOffice] = []
  var searchController: UISearchController!

  override func viewDidLoad() {
    super.viewDidLoad()

    firstly {
      NWSOfficeManager.list()
    }.done { offices in
      self.offices = offices
      self.tableView.reloadData()
    }

    definesPresentationContext = true
    searchController = UISearchController(searchResultsController: nil)
    searchController.searchResultsUpdater = self
    searchController.dimsBackgroundDuringPresentation = false
    navigationItem.searchController = searchController
    navigationItem.hidesSearchBarWhenScrolling = false
  }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension OfficeListViewController: UITableViewDataSource {
  func numberOfSections(in tableView: UITableView) -> Int {
    return 1
  }

  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    if searchController.isActive {
      return searchResults.count
    } else {
      return offices.count
    }
  }

  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
    let office = searchController.isActive ? searchResults[indexPath.row] : offices[indexPath.row]
    cell.textLabel?.text = office.label
    return cell
  }
}

extension OfficeListViewController: UITableViewDelegate {
  func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    let office = searchController.isActive ? searchResults[indexPath.row] : offices[indexPath.row]
    nwsOfficeSelectedDelegate?.onOfficeSelected(office: office)
    navigationController?.popViewController(animated: true)
  }

}

extension OfficeListViewController: UISearchResultsUpdating {
  func updateSearchResults(for searchController: UISearchController) {
    if let text = searchController.searchBar.text, !text.isEmpty {
      self.searchResults = self.offices.filter({ (office) -> Bool in
        if let name = office.name, let stateAbbrev = office.stateAbbrev, let wfo = office.wfo {
          let nameLower = name.lowercased()
          let stateLower = stateAbbrev.lowercased()
          let wfoLower = wfo.lowercased()
          let text = text.lowercased()
          return nameLower.contains(text) || stateLower.contains(text) || wfoLower.contains(text)
        } else {
          return false
        }
      })
    } else {
      self.searchResults = offices
    }

  }
}

protocol NWSOfficeSelectedDelegate : class {
  func onOfficeSelected(office: NWSOffice)
}
