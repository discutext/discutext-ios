# DiscuText iOS App

## Dependencies

- Cocoapods
- XCode 10+
- Swift 4.2+
- Homebrew

## Building

First install a few dependencies via Homebrew: `brew install autoconf automake libtool`. These are required to properly build the GEOSwift dependency.

Run `pod install` then open up the `DiscuText.xcworkspace` file and build and run with XCode normally.

## Adding a Dependency

Add the new dependency to `./Podfile` and then run `pod install`. 

To update all dependencies run `pod update`.

## Other Notes

Carthage was replaced with Cocoapods since GEOSwift is only available via Cocoapods.
